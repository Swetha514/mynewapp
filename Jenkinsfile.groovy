pipeline {
    agent any
    stages {
        stage('check prerequisites') {
            steps {
                sh 'java -version'
                sh 'mavn -version'
            }
        }
	stage('Build') {
	    steps {
		sh 'mvn clean install'
	    }
	}
    }
}
